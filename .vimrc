" version 2.16

set nocompatible " use Vim defaults instead of Vi compatibility

" these are copied+enabled from /etc/vim/vimrc
syntax on
" jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" filetype specific indentation
if has("autocmd")
  filetype plugin indent on
endif

" my editing preferences
set linebreak " break long lines
set breakat=\ \	!@*+;:,./? " the - in -> is not a good linebreak position
set showbreak=» " symbol for linebreak
set display=lastline " show partial lines at the bottom
set modeline " look for modesetting line in opened files
set mousefocus " sadly only works in GUI
set hlsearch " highlight all matches of the search
set incsearch " highlight matches while typing pattern
set title " set terminal title to filename and status
set noshowmatch " don't jump to opening bracket upon writing the closing one
set wildmenu " enhanced command completion
set hidden " allow switching to another buffer when the current one is unsaved
set confirm " ask to save the file on exit
set autowrite " Automatically save before commands like :next and :make
set mouse=a " Enable mouse usage (all modes) in terminals
"set cul " highlight cursor line
"set number " line numbering

" default indenting rules when no editorconfig
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab " spaces instead of tab
set autoindent

set laststatus=2 " always show the status line
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P " default statusline
set statusline=%<%n\ %1*%f%*\ %y\ [%{&ff}]\ %h%m%#ErrorMsg#%r%*%=%-14.(%l,%c%V%)\ %P " fancy
" the difference between StatusLine and StatusLineNC will be applied to this
"   in the inactive window (e.g. remove bold)
" the stating color corresponds to Normal mode
highlight User1 term=inverse,bold cterm=inverse,bold ctermbg=green
" GUI color schemes clear highlighting settings, this autocommand runs after that
autocmd ColorScheme * highlight User1 gui=bold guifg=green guibg=black
" set file name color based on the current mode
autocmd ModeChanged *:[iI]* highlight User1 ctermbg=cyan guifg=cyan
autocmd ModeChanged *:[nN]* highlight User1 ctermbg=green guifg=green
autocmd ModeChanged *:[rR]* highlight User1 ctermbg=red guifg=red
autocmd ModeChanged *:[vVsS]* highlight User1 ctermbg=lightmagenta guifg=lightmagenta
autocmd ModeChanged *:[cC]* highlight User1 ctermbg=yellow guifg=yellow | redraw

if has("gui_running")
    set lines=50 " initial size of gvim
    colorscheme peachpuff
    set guioptions=aegimrLt " remove toolbar (T)
    set guifont=Monospace\ 8
endif

" remove trailing whitespaces before saving files
autocmd FileType c,cpp,java,php,perl,lua,python,glsl,qml,perl6 autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/ containedin=ALL

" Debian puts it in a directory that's not included by default
set runtimepath+=/usr/share/vim-editorconfig

" load tags from upper level directories as well
set tags+=tags;/

" move properly in wrapped lines
nmap <Down> gj
vmap <Down> gj
imap <Down> <C-O>gj
nmap <Up> gk
vmap <Up> gk
imap <Up> <C-O>gk

function GoHome()
    let l:pos = col(".")
    normal! g^
    if col(".") == l:pos
        normal! g0
    endif
endfunction

" home-end behavior adjusted for wrapped lines
" TODO figure out how to call GoHome in visual mode without ending visual mode
nmap <silent> <Home> :call GoHome()<CR>
vmap <silent> <Home> g^
imap <silent> <Home> <C-O>:call GoHome()<CR>
nmap <End> g$
vmap <End> g$
imap <End> <C-O>g$

" scrolling
nmap <C-Down> <C-E>
vmap <C-Down> <C-E>
imap <C-Down> <C-O><C-E>
nmap <C-Up> <C-Y>
vmap <C-Up> <C-Y>
imap <C-Up> <C-O><C-Y>
" I hate the default page jumping
nmap <S-Down> <Down>
vmap <S-Down> <Down>
imap <S-Down> <Down>
nmap <S-Up> <Up>
vmap <S-Up> <Up>
imap <S-Up> <Up>

" convenience inspired by mcedit
nmap <F2> :wall<CR>
imap <F2> <C-O>:wall<CR>
nmap <F10> :q<CR>
vmap <F10> v:q<CR>
imap <F10> <C-O>:q<CR>

" TODO can we run make in another directory?
nmap <F8> :make<CR>

" C-] doesn't work with Hungarian layout
" with 'g' added the tag selector pops up if there are multiple matches
nmap <F9> g<C-]>
nmap <S-F9> <C-W>g<C-]>

" convenient spell checker
nmap <F6> :set spell!<CR>
imap <F6> <C-O>:set spell!<CR>
nmap <F7> z=
imap <F7> <C-O>z=

" NERDTree plugin
nmap <F12> :NERDTreeToggle<CR>

function ChangeGuiFontSize(change)
    if !has("gui_running")
        return
    endif
    let l:size = matchstr(&guifont, "\\d\\+$")
    let l:newsize = l:size + a:change
    let &guifont = substitute(&guifont, "\\d\\+$", l:newsize, "")
    " force the pending redraw to happen before the echo
    redraw | echo "Font size" l:newsize
endfunction

" change font size like in most other applications
nmap <silent> <C-ScrollWheelUp> :call ChangeGuiFontSize(1)<CR>
nmap <silent> <C-ScrollWheelDown> :call ChangeGuiFontSize(-1)<CR>

" do not enter Ex mode
nnoremap Q <nop>
" TODO why is shift+F2 equivalent to Q???
nnoremap <S-F2> <nop>

" no accidental help (use :help)
nmap <F1> <nop>
imap <F1> <nop>
" TODO why is shift+F1 equivalent to P???
"nmap <C-F1> <Help>
