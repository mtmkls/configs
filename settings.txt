Some useful settings:

Bracketed paste: disable printf "\e[?2004l" enable printf "\e[?2004h"
In .bashrc:
PROMPT_COMMAND='printf "\e[?2004l"'

GTK file chooser fix:
dconf write /org/gtk/settings/file-chooser/sort-directories-first true

Vim: this is how you write newline (\n) as a replacement character in s///: <CTRL-K><CTRL-M><CTRL-M>

